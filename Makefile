AS=nasm
ASFLAGS= -f elf64

%.o: %.asm
	$(AS) $(ASFLAGS) -o $@ $<

main: main.o lib.o dict.o
	ld -o $@ $^

.PHONY: clean
clean:
	rm *.o main

