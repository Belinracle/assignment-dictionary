%define LAST_ELEM 0

%macro colon 2

%ifnid %2
	%error "second arg is not identifier"
%endif
%ifnstr %1
	%error "first arg is not string"
%endif

%2: dq LAST_ELEM
db %1, 0

%define LAST_ELEM %2
%endmacro
