%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define BUFF_SIZE 256
%define STDERR 2

global _start

section .rodata
key_not_found: db "Key not found", 10, 0
key_found:     db "Key found here is the value", 10, 0
key_enter:     db "Insert the key: ", 0
key_overflow:  db "Inserted key is too long for buffer or the length is 0", 10, 0

section .text
_start:
	; просим ввести ключ
	mov rdi, key_enter
	call print_string

	; выделяем место на стеке, либо можно поступить иначе и выделить буфер в секции .data
	sub rsp, BUFF_SIZE	; выделяем место на стеке
	mov rdi, rsp		; указываем выделенное место на стеке как буфер
	mov rsi, BUFF_SIZE	; указываем размер выделенного места
	call read_word		; используем готовую функцию, которая вернет 0, если слово слишком большое для буфера, при успехе возвращает адрес буфера в rax, длину слова в rdx.
	cmp rax, 0		; проверяем результат вызова функции
	je .error_read_key
	
	mov rdi, rax		; передаем функции find_word адрес буфера, куда усспешно считали значение
	mov rsi, LAST_ELEM	; передаем указатель на начало словаря
	call find_word
	
	; проверяем результат вызова функции
	test rax, rax
	jz .error_key_not_found	; если не нашли
	add rax, 8		; если нашли сдвигаем указатель потому что там лежит указатель на след элемент, теперь там лежит указатель на строку ключа
	mov rdi, rax 		; кладем в rdi чтобы посчитать длину ключа и на столько сдвинуть, чтобы получить указатель на значение
	push rdi		; сохраняем значение в стеке
	call string_length	; узначем длину ключа
	pop rdi			; достаем сохраненное значение
        inc rdi			; в конце ключа нуль терминатор
        add rdi, rax		; увеличиваем и получаем указатель на значение
        push rdi		; сохраняем чтобы напечатать сообщение
	mov rdi, key_found	
	call print_string
	pop rdi			; достаем указатель на значение и печатаем его
	call print_string
	call print_newline
.stop:
	add rsp, BUFF_SIZE
	call exit

.error_key_not_found:
	mov rdi, key_not_found
	jmp .print_error

.error_read_key:
	mov rdi, key_overflow

.print_error:
	mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rdi, 2
	mov rax, 1
	syscall
	jmp .stop

