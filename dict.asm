global find_word


extern string_equals

section .text
find_word:
	cmp rsi, 0
	je .end
	push rsi
	add rsi, 8
	call string_equals
	pop rsi
	test rax, rax
	jnz .end
	mov rsi, [rsi]
	jmp find_word
.end:
	mov rax, rsi
	ret

