global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .data

newline_char: db 10

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi 
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; div = Unsigned divide RDX:RAX by r/m64, with result stored in RAX := Qotient, RDX := Remainder
; rdi, rsi, rdx, rcx, r8 и r9.
print_uint:
    xor rax, rax
    mov r8, 10          ; divider
    mov r9, 0           ; counter of numbers for correct syscall
    mov rax, rdi        ; copy number to rax to faster work
    .get_last_number_and_put_in_rsp:
        xor rdx, rdx
        div r8          ; divide by divider. quotient->rax, remainder->rdx
        add rdx, 48     ; look in ASCII table and you will see that you need to add 48 positions to remainder to get remainder ascii interpretation

        inc r9          ; increase counter
        dec rsp         ; allocate mem for one char of remainder
        mov [rsp], dl   ; put one char in stack. dl look in https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/x64-architecture
        cmp rax, 0      ; check is there any to divide further
        jne .get_last_number_and_put_in_rsp
    xor rdx,rdx
    mov rax, 1          ; syscall of write
    mov rdx, r9         ; how much bytes to write
    mov rdi, 1          ; stdout
    mov rsi, rsp        ; start of our string in memory 
    syscall
    add rsp, r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0          ; compare number with 0
    jge print_uint      ; if greater or equal just print
    push rdi            ; else save number
    mov rdi, '-'        ; move minus rdi to use print_char
    call print_char     ; print -
    pop rdi             ; pop rdi
    neg rdi             ; make rdi neg
    jmp print_uint      ; print as unsigned, cause we already printed -

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
    xor rax, rax
    .loop:
        mov al, [rdi+r8]
        cmp al, [rsi+r8]
        jne .not_equals
        cmp al, 0
        je .equals
        inc r8
        jmp .loop
.equals:
    mov rax, 1
    ret
.not_equals:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rdi, 0          ; stdin
    mov rdx, 1          ; read 1 byte
    mov rax, 0          ; number of read syscall
    mov rsi, rsp
    syscall
    cmp rax, 1
    mov rax, [rsp]
    je .exit
    mov rax, 0
.exit:
    inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi, rsi, rdx, rcx, r8 и r9
read_word:
    xor rcx, rcx         ; counter of read symbols
    mov r8, rsi          ; save rsi(buffer size) cause we need it in read syscall
    mov r9, rdi          ; save rdi(buffer addr) cause we need it in read syscall
    jmp .loop
.loop:
    push rcx
    call read_char
    pop rcx
    jmp .check
.check:
    inc rcx
    cmp rcx, r8
    jge .overflow
    dec rcx
    cmp rcx, 0
    jg .check_end_of_word
    jmp .pass_whitespaces_at_beginning
.pass_whitespaces_at_beginning:
    cmp al, 0x20
    je .loop
    cmp al, 0x9
    je .loop
    cmp al, 0xA
    je .loop
.check_end_of_word:
    cmp al, 0x4
    je .string_ended
    cmp al, 0
    je .string_ended
    cmp al, 0x20
    je .string_ended
    cmp al, 0x9
    je .string_ended
    cmp al, 0xA
    je .string_ended
    jmp .process
.process:
    mov [r9 + rcx], al
    inc rcx
    jmp .loop
.string_ended:
    mov byte [r9 + rcx], 0
    mov rax, r9
    mov rdx, rcx
    ret
.overflow:
    mov rax, 0
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi, rsi, rdx, rcx, r8 и r9
parse_uint:
    xor rax, rax            ; result
    xor r10, r10            ; counter, we cant use use rdx as counter because mul occurs it
    xor r8, r8              ; clean th reg where we will store numbers
    mov r9, 10              
.read_and_process:
    mov r8b, [rdi + r10]
    cmp r8b, 57
    jg .end
    cmp r8b, 48
    jl .end
    sub r8b, 48
    mul r9
    add rax, r8
    inc r10
    jmp .read_and_process
.end:
    mov rdx, r10
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    call parse_uint
    ret
.negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax    
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    je .overflow
    mov cl, [rdi+rax]
    mov [rsi+rax], cl
    cmp cl, 0
    je .fit
    inc rax
    jmp .loop
.overflow:
    xor rax, rax
.fit:
    ret


;rdi, rsi, rdx, rcx, r8 и r9.
